package com.accenture.service;

import java.util.Date;
import java.util.List;

import com.accenture.beans.EmployeeBean;

public interface EmployeeService {
	public List<EmployeeBean> getEmployeeDetailsWithin(Date startDate, Date endDate) throws Exception;

	public Integer updateEmployeeSalaryWithRole(String role, Double salary) throws Exception;

	public Integer deleteEmployeeByName(String name) throws Exception;
}
