package com.accenture.service;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.beans.EmployeeBean;
import com.accenture.dao.EmployeeDao;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public List<EmployeeBean> getEmployeeDetailsWithin(Date startDate, Date endDate) throws Exception {
		try {
			return employeeDao.getEmployeeDetailsWithin(startDate, endDate);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Integer updateEmployeeSalaryWithRole(String role, Double salary) throws Exception {
		try {
			return employeeDao.updateEmployeeSalaryWithRole(role, salary);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Integer deleteEmployeeByName(String name) throws Exception {
		try {
			return employeeDao.deleteEmployeeByName(name);
		} catch (Exception e) {
			throw e;
		}
	}

}
