package com.accenture.ui;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.accenture.beans.EmployeeBean;
import com.accenture.config.SpringMainConfig;
import com.accenture.service.EmployeeService;

public class UITester {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringMainConfig.class);
		
		EmployeeService service = context.getBean(EmployeeService.class);
		
		
		try {
//			List<EmployeeBean> employeeList = service.getEmployeeDetailsWithin(startDate, endDate);
//			Integer res1 = service.deleteEmployeeByName("manamohan");
			Integer res2 = service.updateEmployeeSalaryWithRole("tester", 999.00);
			
			System.out.println(res2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
