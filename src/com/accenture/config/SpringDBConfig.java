package com.accenture.config;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@PropertySource("classpath:com/accenture/properties/connection.properties")
@EnableTransactionManagement
public class SpringDBConfig {

	@Value("${db.driver}")
	private String driver;

	@Value("${db.url}")
	private String url;

	@Value("${db.user}")
	private String userName;

	@Value("${db.password}")
	private String password;

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public DriverManagerDataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		System.out.println(driver + "\n" + url + "\n" + userName + "\n" + password);
		dataSource.setDriverClassName(driver);
		dataSource.setUrl(url);
		dataSource.setUsername(userName);
		dataSource.setPassword(password);

		return dataSource;
	}

	@Bean
	public JpaVendorAdapter getJpaVendorAdapter() {
		HibernateJpaVendorAdapter vendoreAdapter = new HibernateJpaVendorAdapter();
		vendoreAdapter.setShowSql(true);
		vendoreAdapter.setGenerateDdl(false);
		vendoreAdapter.setDatabasePlatform("org.hibernate.dialect.MySQLDialect");
		System.out.println("SpringDBConfig.getJpaVendorAdapter()");
		return vendoreAdapter;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean getEntityManagerFactory() {
		LocalContainerEntityManagerFactoryBean ef = new LocalContainerEntityManagerFactoryBean();
		ef.setDataSource(getDataSource());
		ef.setJpaVendorAdapter(getJpaVendorAdapter());
		ef.setPackagesToScan("com.accenture.entity");

		return ef;
	}

	@Bean
	public JpaTransactionManager getTransactionManager(EntityManagerFactory factory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager(factory);
//		transactionManager.setEntityManagerFactory(factory);

		System.out.println("SpringDBConfig.getTransactionManager()");
		return transactionManager;
	}
}
