package com.accenture.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "com.accenture")
@Import(SpringDBConfig.class)
public class SpringMainConfig {

}
