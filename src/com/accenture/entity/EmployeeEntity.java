package com.accenture.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Employee")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@NamedQueries({
		@NamedQuery(name = "getEmployeeDetailsWithin", query = "select e from EmployeeEntity e where e.insertTime BETWEEN :startDate and :endDate"),
		@NamedQuery(name = "updateEmployeeSalaryWithRole", query = "update EmployeeEntity e set e.salary = :salary where e.role = :role"),
		@NamedQuery(name = "deleteEmployeeByName", query = "delete from EmployeeEntity e where e.name = :name") })
public class EmployeeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String role;

	@Column(name = "insert_time")
	@Temporal(TemporalType.DATE)
	private Date insertTime;
	private Double salary;
}
