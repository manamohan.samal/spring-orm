package com.accenture.beans;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeBean {
	private Integer id;
	private String name;
	private String role;
	private Date insertDate;
	private Double salary;
}
