package com.accenture.dao;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.beans.EmployeeBean;
import com.accenture.entity.EmployeeEntity;

@Repository
@Transactional
public class EmployeeDaoImpl implements EmployeeDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<EmployeeBean> getEmployeeDetailsWithin(Date startDate, Date endDate) throws Exception {
		List<EmployeeBean> employeeList = null;
		try {
//			employeeList = new ArrayList<>();
			List<EmployeeEntity> employeeListEntity = (List<EmployeeEntity>)entityManager.createNamedQuery("getEmployeeDetailsWithin").getResultList();
			employeeList = employeeListEntity.stream()
												.map(EmployeeDao :: convertEntityToBean)
												.collect(Collectors.toList());
		} catch (Exception e) {
			throw e;
		}
		return employeeList;
	}

	@Override
	public Integer updateEmployeeSalaryWithRole(String role, Double salary) throws Exception {
		Integer result = 0;
		try {
			result = entityManager.createNamedQuery("updateEmployeeSalaryWithRole")
							.setParameter("role", role)
							.setParameter("salary", salary)
							.executeUpdate();
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public Integer deleteEmployeeByName(String name) throws Exception {
		Integer result = 0;
		try {
//			result = entityManager.createNamedQuery("deleteEmployeeByName")
//					.setParameter("name", name)
//					.executeUpdate();
			Query query = entityManager.createNamedQuery("deleteEmployeeByName");
			query.setParameter("name", name);
			result = query.executeUpdate();
					

		} catch (Exception e) {
			throw e;
		}
		return result;
	}
	
	

}
