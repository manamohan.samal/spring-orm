package com.accenture.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.accenture.beans.EmployeeBean;
import com.accenture.entity.EmployeeEntity;

public interface EmployeeDao {
	public List<EmployeeBean> getEmployeeDetailsWithin(Date startDate, Date endDate) throws Exception;

	public Integer updateEmployeeSalaryWithRole(String role, Double salary) throws Exception;

	public Integer deleteEmployeeByName(String name) throws Exception;
	
	public static EmployeeBean convertEntityToBean(EmployeeEntity entity) {
		EmployeeBean bean = new EmployeeBean();
		BeanUtils.copyProperties(entity, bean);
		
		return bean;
	}
	
	public static EmployeeEntity convertBeanToEntity(EmployeeBean bean) {
		EmployeeEntity entity = new EmployeeEntity();
		BeanUtils.copyProperties(bean, entity);
		
		return entity;
	}
}
